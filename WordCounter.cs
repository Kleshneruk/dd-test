using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = { };
            try
            {
                StreamReader sr = new StreamReader("...");
                string line = sr.ReadLine();
                Regex regex;
                while (line != null)
                {
                    line = line.ToLower();
                    regex = new Regex(@"\[(\d*)\]");
                    line = regex.Replace(line, "");
                    regex = new Regex(@"\A(\W*)|(\W*)\z");
                    line = regex.Replace(line, "");
                    regex = new Regex(@"(\W*) (\W*)");
                    line = regex.Replace(line, " ");
                    words = words.Concat(line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)).ToArray();
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            var result = words.GroupBy(x => x).Select(x => new { Word = x.Key, Frequency = x.Count() }).OrderByDescending(x => x.Frequency);
            try
            {
                StreamWriter sw = new StreamWriter("...", false, System.Text.Encoding.UTF8);
                foreach (var item in result)
                {
                    sw.WriteLine(item.Word + "\t" + item.Frequency);
                }
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }
    }
}